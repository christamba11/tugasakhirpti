from . import models
from django import forms

class CreateFriends(forms.ModelForm):
    class Meta:
        model = models.Friends #models yang kita gunakan dari models.py
        fields = [
            'nama','deskripsi','idLine','TTL',
        ]

class CreateComments(forms.ModelForm):
    class Meta:
        model = models.Comment 
        fields = [
            'comment',
        ]