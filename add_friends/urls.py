from . import views
from django.urls import path

app_name = 'add_friends'

urlpatterns = [
    path('', views.friends_list , name='friends_list'),
    path('create_friends/', views.create_friends , name="create_friends"),
    path('details/<int:id>', views.friends_details , name="details"), 
    path('create_comments/<int:id>/', views.create_comments , name="create_comment"),        
]
