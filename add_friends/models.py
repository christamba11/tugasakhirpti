from django.db import models
from django.contrib.auth.models import User

class Friends(models.Model):
    nama = models.CharField(max_length=100)
    deskripsi = models.TextField()
    idLine = models.CharField(max_length=50)
    TTL = models.CharField(max_length=80)  
    author = models.ForeignKey(User,default=None,  on_delete=models.CASCADE,blank=True, null=True)

    def __str__(self):
        return self.nama

class Comment(models.Model):
    comment = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    friend = models.ForeignKey(Friends, on_delete=models.CASCADE, blank=True, null=True, related_name="comment")
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE,blank=True, null=True)
    def __str__(self):
        return "{} - {}".format(self.author, self.comment)

