from django.contrib import admin
from . models import Friends, Comment

# Register your models here.
admin.site.register(Friends)
admin.site.register(Comment)