from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from. models import Friends, Comment
from. import forms
# Create your views here.

def friends_list(request):
    friends = Friends.objects.all().order_by('nama')
    return render(request, 'add_friends/list_friends.html', {'friends':friends})

def friends_details(request, id):
    friends = Friends.objects.get(id = id)
    comment_form = forms.CreateComments()
    return render(request, 'add_friends/friends_details.html', {'friends': friends, 'comment_form':comment_form})

@login_required(login_url="/accounts/login")
def create_friends(request):
    if request.method == 'POST':
        form = forms.CreateFriends(request.POST)
        if form.is_valid():
            #save data into database
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            return redirect('add_friends:friends_list') #name of the URL

    else:
        form = forms.CreateFriends()
    return render(request, 'add_friends/create_friends.html', {'form':form})

@login_required(login_url="/accounts/login")
def create_comments(request, id):
    comment_form = forms.CreateComments(request.POST)
    friend = Friends.objects.get(id = id)
    if request.method=="POST" and comment_form.is_valid():
        instance = comment_form.save(commit=False)
        instance.author = request.user
        instance.friend = friend
        instance.save()
    return redirect('add_friends:details', id = id)
